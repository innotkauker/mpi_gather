#pragma once
#include "mpi.h"

int my_MPI_Gather(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
			void *recvbuf, int recvcount, MPI_Datatype recvtype,
			int root, MPI_Comm comm);
