#include "gather_impl.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>

static int between(int x, int l, int u)
{
	return ((x >= l) && (x <= u));
}

static int sgn(int x)
{
	return (x > 0) - (x < 0);
}

/* gets maximum (and minimum) possible values of indices for other functions */
static void get_corner_coords(int m, int n, int root, int *it, int *ib, int *jt, int *jb)
{
	int rgi = root/m;
	int rgj = root - rgi*m;
	*it = -rgi;
	*jt = -rgj;
	*ib = n - rgi - 1;
	*jb = m - rgj - 1;
}

/* converts linear node index to (i, j) with root as (0, 0)
 *	rank: what to convert,
 *	m: node matrix width,
 *	root: rank of the root node
 *	i, j: where to put indices
 */
static void rank_to_coords(int rank, int m, int root, int *i, int *j)
{
	int gi = rank/m;
	int gj = rank - gi*m;
	int rgi = root/m;
	int rgj = root - rgi*m;
	*i = gi - rgi;
	*j = gj - rgj;
}

/* converts (i, j) back to linear index or -1 if indices are invalid
 *	i, j: indices,
 *	m: node matrix width,
 *	root: rank of the root node
 *	rank: where to put the result
 */
static int coords_to_rank(int i, int j, int m, int n, int root)
{
	int it, ib, jt, jb;
	get_corner_coords(m, n, root, &it, &ib, &jt, &jb);
	if (!between(i, it, ib) || !between(j, jt, jb))
		return -1;
	int rgi = root/m;
	int rgj = root - rgi*m;
	int gi = i + rgi;
	int gj = j + rgj;
	return gi*m + gj;
}

/* nodes are considered children of the one with rank rank
 * if it should wait for data from them before proceeding */
static void get_child_nodes(int rank, int root, int n, int m, int *c0, int *c1, int *c2, int *c3)
{
	int i, j;
	rank_to_coords(rank, n, root, &i, &j);
	if ((i == j) || (i == -j) || (j == -i - 1) || (i == j + 1)) { // diagonal
		if (!i && !j) { // root
			*c0 = coords_to_rank(i - 1, j, m, n, root);
			*c1 = coords_to_rank(i + 1, j, m, n, root);
			*c2 = coords_to_rank(i, j - 1, m, n, root);
			*c3 = coords_to_rank(i, j + 1, m, n, root);
		} else if (!i && (j < 0)) { // (0, -1)
			*c0 = coords_to_rank(i - 1, j, m, n, root);
			*c1 = coords_to_rank(i + 1, j, m, n, root);
			*c2 = coords_to_rank(i, j - 1, m, n, root);
			*c3 = -1;
		} else {
			int t0 = i + sgn(i);
			int t1 = j + (j ? sgn(j) : 1);
			*c0 = coords_to_rank(t0, j, m, n, root);
			*c1 = coords_to_rank(i, t1, m, n, root);
			*c2 = -1;
			*c3 = -1;
		}
	} else { // regular
		if ((abs(j) > i) && (abs(j) > -i))
			*c0 = coords_to_rank(i, j + sgn(j), m, n, root);
		else
			*c0 = coords_to_rank(i + sgn(i), j, m, n, root);
		*c1 = -1;
		*c2 = -1;
		*c3 = -1;
	}
}

/* get rank of a node to which the calling one shall send data */
static int get_parent_node(int rank, int root, int n, int m)
{
	int i;
	int j;
	rank_to_coords(rank, n, root, &i, &j);
	if (j >= 0) {
		if (abs(i) <= j)
			return coords_to_rank(i, j - 1, m, n, root);
		else
			return coords_to_rank(i - sgn(i), j, m, n, root);
	} else {
		if (abs(i) < abs(j))
			return coords_to_rank(i, j + 1, m, n, root);
		else
			return coords_to_rank(i - sgn(i), j, m, n, root);
	}
}

/* puts non-zero parts of the f* buffers into 'to', all buffers are length-sized */
void merge_buffers(void *to, int length, void *f0, void *f1, void *f2, void *f3, void *f4)
{
	int i;
	char *b0 = (char *)f0;
	char *b1 = (char *)f1;
	char *b2 = (char *)f2;
	char *b3 = (char *)f3;
	char *b4 = (char *)f4;
	char *bt = (char *)to;
	for (i = 0 ; i < length; i++)
		bt[i] = bt[i] | (b0 ? b0[i] : 0) | (b1 ? b1[i] : 0) |
			(b2 ? b2[i] : 0) | (b3 ? b3[i] : 0) | (b4 ? b4[i] : 0);
}

/* MPI_Gather implementation
 *	n: row number in node matrix
 */
int my_MPI_Gather(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
			void *recvbuf, int recvcount, MPI_Datatype recvtype,
			int root, MPI_Comm comm)
{
	int myrank, gsize, i, err;
	MPI_Status status;
	int recvtype_size, sendtype_size;
	int n, m;
	int children[4] = {-1, -1, -1, -1};
	void *b[4] = {NULL, NULL, NULL, NULL}; /* where to put received data */
	void *rbuf; /* where to merge received data and what to send to parent */
	void *sbuf; /* and extended sendbuf */

	/* check input */
	if (sendcount != recvcount)
		return MPI_ERR_COUNT;
	if (sendtype != recvtype)
		return MPI_ERR_TYPE;
	if (MPI_Type_size(sendtype, &sendtype_size) || MPI_Type_size(recvtype, &recvtype_size))
		return MPI_ERR_TYPE;
	if (MPI_Comm_rank(comm, &myrank) || MPI_Comm_size(comm, &gsize))
		return MPI_ERR_COMM;
	if (root >= gsize)
		return MPI_ERR_ROOT;

	rbuf = malloc(recvcount*recvtype_size*gsize);
	sbuf = malloc(recvcount*recvtype_size*gsize);

	n = sqrt(gsize);
	m = gsize/n;
	if (m != n) {
		printf("non-square matrices are not supported, sorry\n");
		return MPI_ERR_DIMS;
	}

	/* copy sendbuf to sbuf */
	memset(sbuf, 0, recvcount*recvtype_size*gsize);
	memset(rbuf, 0, recvcount*recvtype_size*gsize);
	memcpy(sbuf + myrank*sendcount*sendtype_size, sendbuf, recvcount*recvtype_size);

	/* receive data from children */
	get_child_nodes(myrank, root, n, m, children, children + 1, children + 2, children + 3);
	MPI_Request request[4] = {-1, -1, -1, -1};
	for (i = 0; i < 4; i++) {
		if (children[i] == -1)
			continue;
		b[i] = malloc(recvcount*recvtype_size*gsize);
		MPI_Irecv(b[i], recvcount*gsize, recvtype, children[i], MPI_ANY_TAG, comm, request + i);
	}
	for (i = 0; i < 4; i++) {
		if (request[i] == -1)
			continue;
		MPI_Wait(request + i, &status);
	}
	merge_buffers(rbuf, recvcount*recvtype_size*gsize, b[0], b[1], b[2], b[3], sbuf);
	for (i = 0; i < 4; i++)
		if (b[i])
			free(b[i]);

	if (myrank == root) {
		/* write operation result */
		memcpy(recvbuf, rbuf, recvcount*recvtype_size*gsize);
	} else {
		/* send data to parent node */
		MPI_Send(rbuf, sendcount*gsize, sendtype, get_parent_node(myrank, root, n, m), 1, comm);
	}
	free(rbuf);
	free(sbuf);
	return 0;
}
