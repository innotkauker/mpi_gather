# Custom MPI Gather imlpementation #

This repository contains an imlpementation of MPI Gather
operation for a [transputer](https://en.wikipedia.org/wiki/Transputer)-based
cluster.

```C
int my_MPI_Gather(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
			void *recvbuf, int recvcount, MPI_Datatype recvtype,
			int root, MPI_Comm comm);
```

### Algorithm description ###

Each process calculates a list of nodes which should send data to it.
After that, an asynchronous receiving operation is initialized for each of them.
Upon successful reception the data is combined in a single buffer,
which is transmitted further.
The scheme according to which transmission direction is determined
is shown below on an example of 5x5 node matrix. `x` is the root node.

	v v v v <
	> v v < <
	> > x < <
	> ^ ^ < <
	^ ^ ^ ^ <

The amound of sent data is always equal to `recvbuf`.
If some part of it is unknown, it is replaced with zeroes.
When a node receives all buffers sent to it, it combines them using bitwise OR.
This way the root node gets its buffer filled with all the requested data.

### Time complexity ###

The example below shows the most complex case for a given NxN transputer matrix.

	х < < < <
	^ < < < <
	^ ^ < < <
	^ ^ ^ < <
	^ ^ ^ ^ <

If a node needs `Ts` _days_ to start the transmission and it takes
`Tb` _days_ to send a single byte, and `L` is length (in bytes) of the buffer,
the whole operation will take `2*(N-1)*(Ts+N*N*L*Tb)` _days_.

### Contents ##

* **gather_impl.c** - my_MPI_Gather implementation.
* **gather_impl.h** - the related header.
* **test.c** - contains some tests.
* **Makefile** - a Makefile to build the function and the tests.

