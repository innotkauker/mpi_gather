default: build

build:
	mpicc -g -O3 -lm ./gather_impl.c ./test.c

run:
	mpirun -np 25 ./a.out

all: build run
